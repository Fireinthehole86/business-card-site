import styled from "styled-components";


export const HireCardContainer=styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    gap: 2rem;
    min-width: 100vw;
	scroll-snap-align: start;
	text-align: center;
	position: relative;
    background: rgba(255,255,255, 0.4);
    box-shadow: 0px 8px 32px 0px rgba(31,38,135,0.37);
    border-radius: 10px;
    padding: 1rem;
    border: 0px;
    @media screen and (min-width: 920px){
        justify-content: center;
    }
`;

export const Title = styled.h2`
    text-transform: uppercase;
`

export const Subtitle = styled.h3`
    font-size: 2rem;
`;

export const HireCardImage = styled.img`
    width: 90%;
    @media screen and (min-width: 920px){
        width: 30%;
    }
    border-radius: 24px;
    
`;

export const HireCardLink = styled.a`
    color: #fff;
    font-size: 3rem;
    padding: 1rem;
    min-width: 90%;
    @media screen and (min-width: 920px){
        min-width: 30%;
    }
    background-color: #8cc6e9;
    text-decoration: none;
`;
