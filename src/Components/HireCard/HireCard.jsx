import React from "react";
import * as Styled from "./HireCard.styled";


const HireCard = (props) => {

    const MailTo = ({ email = "", subject = "", body = "", ...props }) => {
        
        return (
          <Styled.HireCardLink href={`mailto:${email}?subject=${subject}&body=${body}`}>
            {props.children}
          </Styled.HireCardLink>
        );
      }


    return (
        <Styled.HireCardContainer>
            <Styled.Title>{props.name}</Styled.Title>
            {props.imageURL && (<Styled.HireCardImage alt="hire this person" src={props.imageURL}/>)}
            <Styled.Subtitle>{props.jobTitle}</Styled.Subtitle>
            {props.linkedIn && (<Styled.HireCardLink target="_blank" rel="noreferrer" href={props.linkedIn}>LinkedIn</Styled.HireCardLink>)}
            {props.email && (<MailTo email={props.email} subject="Hey! I was talking to Andy at Develop:Brighton and you sound awesome! Let's connect!" body="If you don't hear from me, grab my email address from this email and follow-up with me in a couple of days or so about potential opportunities. Thanks!">Email</MailTo>)}
            {props.website && (<Styled.HireCardLink target="_blank" rel="noreferrer" href={props.website}>Website</Styled.HireCardLink>)}
            <p>Swipe left/right</p>
        </Styled.HireCardContainer>
    )
}

export default HireCard;