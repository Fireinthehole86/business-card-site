import React from "react";
import HireCard from "../HireCard/HireCard";
import * as Styled from "./HireCardContainer.styled";

const HireCardContainer = (props) => {

    return (
        <Styled.CardContainer>
            {props.data.map((card) => (
                <HireCard 
                    key={card.name}
                    name={card.name} 
                    imageURL={card.photo} 
                    jobTitle={card.jobTitle}
                    linkedIn={card.linkedIn}
                    email={card.email}
                    website={card.website}    
                />
            ))}
        </Styled.CardContainer>
    )
}

export default HireCardContainer;