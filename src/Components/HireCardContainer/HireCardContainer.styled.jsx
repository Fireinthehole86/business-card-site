import styled from "styled-components";

export const CardContainer = styled.div`
    scroll-snap-type: x mandatory;
    display: flex;
    overflow-x: scroll;
    gap: 2rem;
`;