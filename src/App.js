import './App.css';
import HireCardContainer from './Components/HireCardContainer/HireCardContainer';
import Data from "./data/data.json";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className='App-header-inner'>
          <HireCardContainer data={Data}/>
        </div>
      </header>
    </div>
  );
}

export default App;
